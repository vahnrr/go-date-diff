package main

import (
	"fmt"
	"os"
	"strings"
	"time"
)

// acceptedFormats represents the input formats this utility understands.
//
// NOTE: Their order is purely based on which ones I use most.
var acceptedFormats = []string{
	time.DateOnly,
	time.DateTime,
	time.TimeOnly,
	time.RFC3339,
	time.Stamp,
	time.ANSIC,
	time.UnixDate,
}

// unitSeparator represents what separates time units for the output.
const unitSeparator = ", "

func main() {
	var to, from time.Time

	// Requires at least one argument.
	if (len(os.Args) < 2) {
		fmt.Println("Error: FROM argument is missing.")
		usage()
	}

	// Handle common help options.
	if (os.Args[1] == "-h") || (os.Args[1] == "-help") || (os.Args[1] == "--help") {
		usage()
	}

	if len(os.Args) > 1 {
		from = parseTime(os.Args[1])

		// Either 'to' is provided as the second argument or the current time is used as a fallback.
		if len(os.Args) > 2 {
			to = parseTime(os.Args[2])
		} else {
			to = time.Now()
		}
	}

	var builder strings.Builder

	diff := to.Sub(from).Hours()
	years := int(diff / 24 / 365)
	builder.WriteString(format(years, "year"))

	diff  -= float64(years * 24 * 365)
	months := int(diff / 24 / 30)
	builder.WriteString(format(months, "month"))

	diff -= float64(months * 24 * 30)
	weeks := int(diff / 24 / 7)
	builder.WriteString(format(weeks, "week"))

	diff -= float64(weeks * 24 * 7)
	days := int(diff / 24)
	builder.WriteString(format(days, "day"))

	diff -= float64(days * 24)
	hours := int(diff)
	builder.WriteString(format(hours, "hour"))

	diff -= float64(hours)
	minutes := int(diff * 60)
	builder.WriteString(format(minutes, "minute"))

	diff -= float64(minutes) / 60.0
	seconds := int(diff * 60 * 60)
	builder.WriteString(format(seconds, "second"))

	description := builder.String()
	description = strings.TrimSuffix(description, unitSeparator)

	fmt.Println(description)
}

func usage() {
	fmt.Println(`Usage: date-diff FROM [TO]

Calculate the human-readable amount of time that passed between FROM and TO.
TO can be omitted, in that case the current time is used.

NOTE: FROM and TO don't need to be ordered by age, the programme automatically
determines which one is older before any calculation is made.

Accepted date/time formats include:`)
	for _, f := range acceptedFormats {
		fmt.Printf("- %s\n", f)
	}
	os.Exit(1)
}

// parseTime returns the given value as a time that can follows several formats.
func parseTime(value string) time.Time {
	var t time.Time
	var err error

	// Try and parse the given value using the accepted formats.
	for _, f := range acceptedFormats {
		t, err = time.Parse(f, value)

		// Successful parsing, don't bother trying to parse other formats.
		if err == nil {
			break
		}
	}

	if err != nil {
		fmt.Printf("Error: Unknown format for input: %s\n", value)
		usage()
	}

	return t
}

// format returns the description of the given value and its unit.
func format(value int, unit string) string {
	var out string

	// Use the absolute value when a negative is encountered.
	if value < 0 {
		value = value * -1
	}

	if value > 0 {
		out = fmt.Sprintf("%d %s", value, unit)

		// Show that value may be plural.
		if value > 1 {
			out += "s"
		}

		out += unitSeparator
	}

	return out
}
