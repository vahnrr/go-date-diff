# Explicitly declare these as rules not files to draw the dependency tree with.
.PHONY: help clean build

# PATH directory where binaries should be dumped.
BIN = ${XDG_BIN_HOME}
# Fallback to a sane default in case the XDG variable wasn't set.
ifeq ($(BIN),)
	BIN = ${HOME}/.local/bin
endif

# Final binary's path.
OUT = $(BIN)/date-diff

help: ## Show this help message.
	$(info # $@)
# Retrieve any documented task, alphabetically sort them then list them out, format:
# <task-name>: [<task-dependencies> ]## <task-documentation>
#
# NOTE: Field separator (-F) matches everything between the task's name to its documentation, so
# that tasks' that declare dependencies can still be documented.
	@awk -F ':.*? ## ' '/^[a-zA-Z0-9_-]+:.*? ## .*$$/ { printf "- %-20s %s\n", $$1, $$2 }' \
		$(MAKEFILE_LIST) | sort

clean: ## Remove cached/object files.
	$(info # $@)
	go clean ./...
	rm -f $(OUT)

build: ## Build the final binary.
	$(info # $@)
	mkdir -p $(BIN)
	go build -o $(OUT)
