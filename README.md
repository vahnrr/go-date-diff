> A rudimentory utility to calculate the time difference between 2 dates.

```shell
$ date-diff '2022-04-06 11:38:24' '2023-06-17 18:49:12'
1 year, 2 months, 1 week, 5 days, 7 hours, 10 minutes, 48 seconds
```

# Building

A convenient `make help` task describes the ways the project can be built and cleaned.

# Accepted date formats

See the usage from the `date-diff -h` command.

# Warning

This is a very rudimentory utility, it may be incorrect for long differences between 2 dates.

A more robust implementation would consider leap years and such.
This is a quick and dirty way to calculate some duration for casual context.
